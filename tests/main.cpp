#include <rili/Test.hpp>
#include <string>

int main(int, char**) {
    return rili::test::runner::run(
               [](std::string const&, std::string const&) { return rili::test::runner::FilteringResult::Run; })
               ? 0
               : 1;
}
