#include <rili/Test.hpp>
#include <rili/service/Compute.hpp>
#include <rili/service/Time.hpp>
#include <thread>

TEST(Service_Compute, SimpleValue_resolve) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule<int>([tid](rili::Promise<int>::OnResolveHandler const& resolve) {
                EXPECT_NE(tid, std::this_thread::get_id());
                resolve(7);
            })
            .Then(
                [tid](int const& v) {
                    EXPECT_EQ(tid, std::this_thread::get_id());
                    EXPECT_EQ(v, 7);
                },
                [](rili::Promise<int>::FailureType const&) { ADD_FAILURE(); });
    });
}

TEST(Service_Compute, SimpleValue_throws) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule<int>([tid](rili::Promise<int>::OnResolveHandler const&) {
                EXPECT_NE(tid, std::this_thread::get_id());
                throw 5;
            })
            .Then([](int const&) { ADD_FAILURE(); },
                  [tid](rili::Promise<int>::FailureType const& e) {
                      EXPECT_EQ(tid, std::this_thread::get_id());
                      try {
                          std::rethrow_exception(e);
                      } catch (int& v) {
                          EXPECT_EQ(v, 5);
                      } catch (...) {
                          ADD_FAILURE();
                      }
                  });
    });
}

TEST(Service_Compute, SimpleVoid_resolve) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule([tid](rili::Promise<void>::OnResolveHandler const& resolve) {
                EXPECT_NE(tid, std::this_thread::get_id());
                resolve();
            })
            .Then([tid]() { EXPECT_EQ(tid, std::this_thread::get_id()); },
                  [](rili::Promise<void>::FailureType const&) { ADD_FAILURE(); });
    });
}

TEST(Service_Compute, SimpleVoid_throws) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule([tid](rili::Promise<void>::OnResolveHandler const&) {
                EXPECT_NE(tid, std::this_thread::get_id());
                throw 5;
            })
            .Then([]() { ADD_FAILURE(); },
                  [tid](rili::Promise<void>::FailureType const& e) {
                      EXPECT_EQ(tid, std::this_thread::get_id());
                      try {
                          std::rethrow_exception(e);
                      } catch (int& v) {
                          EXPECT_EQ(v, 5);
                      } catch (...) {
                          ADD_FAILURE();
                      }
                  });
    });
}

TEST(Service_Compute, ComplexValue_resolve) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule<int>(
                [tid](rili::Promise<int>::OnResolveHandler const& resolve, rili::Promise<int>::OnRejectHandler const&) {
                    EXPECT_NE(tid, std::this_thread::get_id());
                    resolve(7);
                })
            .Then(
                [tid](int const& v) {
                    EXPECT_EQ(tid, std::this_thread::get_id());
                    EXPECT_EQ(v, 7);
                },
                [](rili::Promise<int>::FailureType const&) { ADD_FAILURE(); });
    });
}

TEST(Service_Compute, ComplexValue_rejects) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule<int>(
                [tid](rili::Promise<int>::OnResolveHandler const&, rili::Promise<int>::OnRejectHandler const& reject) {
                    EXPECT_NE(tid, std::this_thread::get_id());
                    reject(std::make_exception_ptr(7));
                })
            .Then([](int const&) { ADD_FAILURE(); },
                  [tid](rili::Promise<int>::FailureType const& e) {
                      EXPECT_EQ(tid, std::this_thread::get_id());
                      try {
                          std::rethrow_exception(e);
                      } catch (int& v) {
                          EXPECT_EQ(v, 7);
                      } catch (...) {
                          ADD_FAILURE();
                      }
                  });
    });
}

TEST(Service_Compute, ComplexValue_throws) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule<int>(
                [tid](rili::Promise<int>::OnResolveHandler const&, rili::Promise<int>::OnRejectHandler const&) {
                    EXPECT_NE(tid, std::this_thread::get_id());
                    throw 7;
                })
            .Then([](int const&) { ADD_FAILURE(); },
                  [tid](rili::Promise<int>::FailureType const& e) {
                      EXPECT_EQ(tid, std::this_thread::get_id());
                      try {
                          std::rethrow_exception(e);
                      } catch (int& v) {
                          EXPECT_EQ(v, 7);
                      } catch (...) {
                          ADD_FAILURE();
                      }
                  });
    });
}

TEST(Service_Compute, ComplexVoid_resolve) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule([tid](rili::Promise<void>::OnResolveHandler const& resolve,
                            rili::Promise<void>::OnRejectHandler const&) {
                EXPECT_NE(tid, std::this_thread::get_id());
                resolve();
            })
            .Then([tid]() { EXPECT_EQ(tid, std::this_thread::get_id()); },
                  [](rili::Promise<void>::FailureType const&) { ADD_FAILURE(); });
    });
}

TEST(Service_Compute, ComplexVoid_rejects) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule([tid](rili::Promise<void>::OnResolveHandler const&,
                            rili::Promise<void>::OnRejectHandler const& reject) {
                EXPECT_NE(tid, std::this_thread::get_id());
                reject(std::make_exception_ptr(7));
            })
            .Then([]() { ADD_FAILURE(); },
                  [tid](rili::Promise<void>::FailureType const& e) {
                      EXPECT_EQ(tid, std::this_thread::get_id());
                      try {
                          std::rethrow_exception(e);
                      } catch (int& v) {
                          EXPECT_EQ(v, 7);
                      } catch (...) {
                          ADD_FAILURE();
                      }
                  });
    });
}

TEST(Service_Compute, ComplexVoid_throws) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        auto tid = std::this_thread::get_id();
        service
            .schedule([tid](rili::Promise<void>::OnResolveHandler const&, rili::Promise<void>::OnRejectHandler const&) {
                EXPECT_NE(tid, std::this_thread::get_id());
                throw 7;
            })
            .Then([]() { ADD_FAILURE(); },
                  [tid](rili::Promise<void>::FailureType const& e) {
                      EXPECT_EQ(tid, std::this_thread::get_id());
                      try {
                          std::rethrow_exception(e);
                      } catch (int& v) {
                          EXPECT_EQ(v, 7);
                      } catch (...) {
                          ADD_FAILURE();
                      }
                  });
    });
}

TEST(Service_Compute, OtherServicesAvaliableInNewComputeThread) {
    bool wasChecked = false;
    auto tid = std::this_thread::get_id();

    rili::Context::run([&wasChecked, tid]() {
        auto& service = rili::service::Compute::get();
        auto now(std::chrono::steady_clock::now());
        service
            .schedule([tid](rili::Promise<void>::OnResolveHandler const& resolve,
                            rili::Promise<void>::OnRejectHandler const& reject) {
                EXPECT_NE(tid, std::this_thread::get_id());
                rili::service::Time::get()
                    .promiseFor(std::chrono::milliseconds(10 * TIME_SCALE_FACTOR))
                    .Then(resolve, reject);
            })
            .Then(
                [tid, now, &wasChecked]() {
                    EXPECT_EQ(tid, std::this_thread::get_id());
                    EXPECT_GE(std::chrono::steady_clock::now(),
                              now + std::chrono::milliseconds(10 * TIME_SCALE_FACTOR));
                    EXPECT_LE(std::chrono::steady_clock::now(),
                              now + std::chrono::milliseconds(30 * TIME_SCALE_FACTOR));
                    wasChecked = true;
                },
                [](rili::Promise<void>::FailureType const&) { ADD_FAILURE(); });
    });
    EXPECT_TRUE(wasChecked);
}

TEST(Service_Compute, MultipleThreadsCanBeAssignedInOneTime) {
    rili::Context::run([]() {
        auto& service = rili::service::Compute::get();
        rili::Promise<void> promise;
        for (int i = 0; i < 5; i++) {
            promise = service.schedule([](rili::Promise<void>::OnResolveHandler const& resolve) {
                std::this_thread::sleep_for(std::chrono::milliseconds(10 * TIME_SCALE_FACTOR));
                resolve();
            });
        }
        promise.Then([]() {}, [](rili::Promise<void>::FailureType const&) { ADD_FAILURE(); });
    });
}

TEST(Service_Compute, ThreadsCanCreateThreadsRecursively) {
    rili::Context::run([]() {
        rili::service::Compute::get()
            .schedule([](rili::Promise<void>::OnResolveHandler const& resolve) {
                rili::service::Compute::get()
                    .schedule([](rili::Promise<void>::OnResolveHandler const& resolve) {
                        rili::service::Compute::get()
                            .schedule([](rili::Promise<void>::OnResolveHandler const& resolve) {
                                rili::service::Compute::get()
                                    .schedule([](rili::Promise<void>::OnResolveHandler const& resolve) {
                                        rili::service::Compute::get()
                                            .schedule([](rili::Promise<void>::OnResolveHandler const& resolve) {
                                                rili::service::Compute::get()
                                                    .schedule([](rili::Promise<void>::OnResolveHandler const& resolve) {
                                                        resolve();
                                                    })
                                                    .Then(resolve);
                                            })
                                            .Then(resolve);
                                    })
                                    .Then(resolve);
                            })
                            .Then(resolve);
                    })
                    .Then(resolve);
            })
            .Then([]() {}, [](rili::Promise<void>::FailureType const&) { ADD_FAILURE(); });
    });
}
