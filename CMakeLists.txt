cmake_minimum_required(VERSION 3.0.0)
project(rili-service-compute)
cmake_policy(SET CMP0054 NEW)

unset(MSVC)
if (${CMAKE_CXX_COMPILER_ID} STREQUAL "AppleClang" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wmissing-declarations -fno-rtti -fexceptions -fstack-protector -pedantic -Wall -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdeprecated -Werror -Wextra -Winit-self -Wnon-virtual-dtor -Wno-padded -Wno-shadow -Wno-unused-macros -Wold-style-cast -Wpedantic -Wpointer-arith -Wsign-promo -Wstrict-aliasing -Wstrict-overflow=5 -Wuninitialized")
elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL "MSVC")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /bigobj")
else()
  message(FATAL_ERROR "Unsupported compiler:${CMAKE_CXX_COMPILER_ID}")
endif()

option(RILI_SERVICE_COMPUTE_COVERAGE "If enabled will be compiled with coverage options" OFF)
option(RILI_SERVICE_COMPUTE_TESTS "If enabled tests will be build" OFF)
option(RILI_SERVICE_COMPUTE_TESTS_WITH_VALGRIND "If enabled tests will be build with support for valgrind" OFF)

if(RILI_SERVICE_COMPUTE_COVERAGE)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
endif()

set(SOURCES
  rili/service/Compute.cpp
  rili/service/Compute.hpp
)

set(TESTS
  tests/main.cpp
  tests/Compute.cpp
)

find_package(Threads REQUIRED)
find_package(rili-compatibility REQUIRED)
find_package(rili-promise REQUIRED)
include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${rili-compatibility_INCLUDE_DIRS} ${rili-promise_INCLUDE_DIRS})

add_library(${PROJECT_NAME} STATIC ${SOURCES})
target_link_libraries(${PROJECT_NAME} rili-compatibility rili-promise ${CMAKE_THREAD_LIBS_INIT})

install(TARGETS ${PROJECT_NAME}
  EXPORT ${PROJECT_NAME}Targets
  ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" COMPONENT staticlib)
install(DIRECTORY rili DESTINATION "${CMAKE_INSTALL_PREFIX}/include" FILES_MATCHING PATTERN "*.hpp")

set(INSTALL_CMAKE_CONFIG_DIR "${CMAKE_INSTALL_PREFIX}/lib/cmake/${PROJECT_NAME}")
configure_file("${CMAKE_CURRENT_SOURCE_DIR}/config.cmake.in" "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake" @ONLY)
install(EXPORT ${PROJECT_NAME}Targets DESTINATION "${INSTALL_CMAKE_CONFIG_DIR}" COMPONENT dev)
install(FILES "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake" DESTINATION "${INSTALL_CMAKE_CONFIG_DIR}" COMPONENT dev)

if(RILI_SERVICE_COMPUTE_TESTS)
  if(RILI_SERVICE_COMPUTE_TESTS_WITH_VALGRIND)
    add_definitions(-DTIME_SCALE_FACTOR=100)
  else()
    add_definitions(-DTIME_SCALE_FACTOR=1)
  endif()

  find_package(rili-test REQUIRED)
  find_package(rili-service-time REQUIRED)
  include_directories(tests ${rili-test_INCLUDE_DIRS} ${rili-service-time_INCLUDE_DIRS})

  add_executable(${PROJECT_NAME}_tests ${TESTS})
  target_link_libraries(${PROJECT_NAME}_tests ${PROJECT_NAME} rili-test rili-service-time)
endif()

set(CONFIG_FILES
  .clang-format
  .gitlab-ci.yml
  .hell.json
  .gitignore
  Doxyfile
  LICENSE.md
  README.md
)

add_custom_target(${PROJECT_NAME}-IDE SOURCES ${CONFIG_FILES})
