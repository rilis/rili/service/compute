# Rili Compute service

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation
of compute service - it provide API which allow to create with Promise API threads with attached rili::Context

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/service/compute)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/service/compute/badges/master/build.svg)](https://gitlab.com/rilis/rili/service/compute/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/service/compute/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/service/compute/commits/master)
