#pragma once
/** @file */

#include <exception>
#include <memory>
#include <rili/Promise.hpp>

namespace rili {
namespace service {

/**
 * @brief The rili::service::ComputeBase class provide interface to schedule asynchronous long runing task in another
 * thread to not block current.
 *
 * User provided callback will be executed in another thread and returned rili::Promise
 * will be resolved/rejected depending on user callback decision. In case when user callback will throw, returned
 * rili::Promise will be rejected with thrown exception as a rejection reason.
 *
 * @remark is interface class
 *
 * @see rili::service::Compute
 * @see rili::Promise
 * @see rili::Promise<void>
 */
class ComputeBase {
 public:
    /**
     * @brief is type in which all kinds of rejections / exceptions are stored (same like in rili::Promise)
     */
    typedef std::exception_ptr FailureType;
    virtual ~ComputeBase() = default;

 protected:
    /// @cond INTERNAL
    virtual void assignThreadAndRun(std::function<void()> const& cb) = 0;
    /// @endcond

 public:
    /**
     * @brief used to run user callback in another thread context.
     *
     * Callback will be called with parameters "resolve" and "reject" (same like in
     * rili::Promise::Promise(ComplexInitializer const&)). When resolve will be called then returned rili::Promise will
     * be resolved with given value. When reject will be called then returned rili::Promise will be rejected with given
     * value. If initializer throws then returned rili::Promise will be rejected with thrown exception as rejection
     * reason.
     *
     * @param initializer is function which is executed to resolve or reject rili::Promise. It provide as first
     * parameter another function which can be used to resolve rili::Promise and as second function which can be used to
     * reject rili::Promise.
     *
     * @remark This function is thread safe.
     * @remark This function is not blocking.
     *
     * @return rili::Promise related to compute task
     */
    template <typename PromisedType>
    rili::Promise<PromisedType> schedule(
        typename rili::Promise<PromisedType>::ComplexInitializer const& initializer) noexcept {
        return rili::Promise<PromisedType>(
            [this, initializer](typename rili::Promise<PromisedType>::OnResolveHandler const& resolve,
                                typename rili::Promise<PromisedType>::OnRejectHandler const& reject) {
                this->assignThreadAndRun([initializer, resolve, reject]() {
                    try {
                        Context::run([&initializer, &resolve, &reject]() { initializer(resolve, reject); });
                    } catch (...) {
                        reject(std::current_exception());
                    }
                });
            });
    }

    /**
     * @brief used to run user callback in another thread context.
     *
     * Callback will be called with parameter "resolve" (same like in rili::Promise::Promise(BasicInitializer const&)).
     * When resolve will be called then returned rili::Promise will be resolved with given value. If initializer throws
     * then returned rili::Promise will be rejected with thrown exception as rejection reason.
     *
     * @param initializer is function which is executed to resolve rili::Promise. It provide as parameter another
     * function which can be used to resolve rili::Promise.
     *
     * @remark This function is thread safe.
     * @remark This function is not blocking.
     *
     * @return rili::Promise related to compute task
     */
    template <typename PromisedType>
    rili::Promise<PromisedType> schedule(
        typename rili::Promise<PromisedType>::BasicInitializer const& initializer) noexcept {
        return rili::Promise<PromisedType>(
            [this, initializer](typename rili::Promise<PromisedType>::OnResolveHandler const& resolve,
                                typename rili::Promise<PromisedType>::OnRejectHandler const& reject) {
                this->assignThreadAndRun([initializer, resolve, reject]() {
                    try {
                        Context::run([&initializer, &resolve]() { initializer(resolve); });
                    } catch (...) {
                        reject(std::current_exception());
                    }
                });
            });
    }

    /**
     * @brief used to run user callback in another thread context.
     *
     * Callback will be called with parameter "resolve" (same like in rili::Promise<void>::Promise(BasicInitializer
     * const&)). When resolve will be called then returned rili::Promise<void> will be resolved. If initializer throws
     * then returned rili::Promise<void> will be rejected with thrown exception as rejection reason.
     *
     * @param initializer is function which is executed to resolve rili::Promise<void>. It provide as parameter another
     * function which can be used to resolve rili::Promise<void>.
     *
     * @remark This function is thread safe.
     * @remark This function is not blocking.
     *
     * @return rili::Promise<void> related to compute task
     */
    rili::Promise<void> schedule(typename rili::Promise<void>::BasicInitializer const& initializer) noexcept;

    /**
     * @brief used to run user callback in another thread context.
     *
     * Callback will be called with parameters "resolve" and "reject" (same like in
     * rili::Promise<void>::Promise(ComplexInitializer const&)). When resolve will be called then returned
     * rili::Promise<void> will be resolved with given value. When reject will be called then returned
     * rili::Promise<void> will be rejected with given value. If initializer throws then returned rili::Promise<void>
     * will be rejected with thrown exception as rejection reason.
     *
     * @param initializer is function which is executed to resolve or reject rili::Promise<void>. It provide as first
     * parameter another function which can be used to resolve rili::Promise<void> and as second function which can be
     * used to reject rili::Promise<void>.
     *
     * @remark This function is thread safe.
     * @remark This function is not blocking.
     *
     * @return rili::Promise<void> related to compute task
     */
    rili::Promise<void> schedule(typename rili::Promise<void>::ComplexInitializer const& initializer) noexcept;
};

/**
 * @brief The Compute class is default ComputeBase service implementation.
 */
class Compute final : public ComputeBase {
 public:
    /**
     * @brief Get or create rili::service::Compute instance.
     *
     * New rili::service::Compute is created if was not created earlier. If rili::service::Compute was created
     * previously return existing instance.
     *
     * @remark This function is thread safe.
     * @remark This function is not blocking.
     *
     * @return Instance of rili::service::Compute
     */
    static Compute& get() noexcept;

    virtual ~Compute() = default;

 protected:
    /// @cond INTERNAL
    void assignThreadAndRun(std::function<void()> const& cb) override;
    /// @endcond

 private:
    Compute();
    Compute(Compute const& other) = delete;
    Compute& operator=(Compute const&) = delete;

 private:
    class Impl;
    std::unique_ptr<Impl> m_impl;
};

}  // namespace service
}  // namespace rili
