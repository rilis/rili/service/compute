#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <list>
#include <memory>
#include <mutex>
#include <rili/MakeUnique.hpp>
#include <rili/service/Compute.hpp>
#include <thread>

namespace rili {
namespace service {
class Compute::Impl {
 public:
    typedef std::chrono::steady_clock Clock;

    void worker(std::shared_ptr<std::thread> self) noexcept {
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            ++m_numberOfWorkers;
        }
        constexpr std::chrono::milliseconds deadlineDuration(50);
        Clock::time_point deadline = Clock::now() + deadlineDuration;

        for (;;) {
            Task task;
            {
                std::unique_lock<std::mutex> lock(m_mutex);
                if (!m_tasks.empty()) {
                    task = m_tasks.front();
                    m_tasks.pop_front();
                    m_cv.notify_one();
                } else if (deadline > Clock::now()) {
                    ++m_numberOfIdleWorkers;
                    m_cv.wait_until(lock, deadline);
                    --m_numberOfIdleWorkers;
                } else {
                    break;
                }
            }
            if (task) {
                task();
                deadline = Clock::now() + deadlineDuration;
            }
        }

        std::lock_guard<std::mutex> lock(m_mutex);
        m_tasks.push_back([self]() { self->join(); });
        --m_numberOfWorkers;
        m_cv.notify_one();
    }

    Impl() : m_mutex(), m_cv(), m_tasks(), m_numberOfWorkers(0), m_numberOfIdleWorkers(0), m_stop(false) {}

    ~Impl() {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_stop = true;
        for (;;) {
            if (m_numberOfWorkers != 0) {
                m_cv.notify_one();
                m_cv.wait(lock);
            } else {
                break;
            }
        }
        for (auto& task : m_tasks) {
            task();
        }
    }
    void assignThreadAndRun(std::function<void()> const& cb) {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_stop) {
            throw std::runtime_error(
                "rili::service::Compute::Impl: Creating new threads is not allowed during application teardown!");
        }
        m_tasks.push_back(cb);
        if (m_numberOfIdleWorkers >= m_tasks.size()) {
            m_cv.notify_one();
        } else {
            auto holder(std::make_shared<std::thread>());
            std::thread(&Compute::Impl::worker, this, holder).swap(*holder);
        }
    }

 private:
    typedef std::function<void()> Task;
    std::mutex m_mutex;
    std::condition_variable m_cv;
    std::list<Task> m_tasks;
    std::uint_fast32_t m_numberOfWorkers;
    std::uint_fast32_t m_numberOfIdleWorkers;
    bool m_stop;
};

Compute& Compute::get() noexcept {
    static Compute instance;
    return instance;
}

void Compute::assignThreadAndRun(std::function<void()> const& cb) { m_impl->assignThreadAndRun(cb); }

Compute::Compute() : ComputeBase(), m_impl(rili::make_unique<Impl>()) {}

rili::Promise<void> ComputeBase::schedule(
    typename rili::Promise<void>::ComplexInitializer const& initializer) noexcept {
    return rili::Promise<void>([this, initializer](rili::Promise<void>::OnResolveHandler const& resolve,
                                                   rili::Promise<void>::OnRejectHandler const& reject) {
        this->assignThreadAndRun([initializer, resolve, reject]() {
            try {
                Context::run([&initializer, &resolve, &reject]() { initializer(resolve, reject); });
            } catch (...) {
                reject(std::current_exception());
            }
        });
    });
}

rili::Promise<void> ComputeBase::schedule(typename rili::Promise<void>::BasicInitializer const& initializer) noexcept {
    return rili::Promise<void>([this, initializer](rili::Promise<void>::OnResolveHandler const& resolve,
                                                   rili::Promise<void>::OnRejectHandler const& reject) {
        this->assignThreadAndRun([initializer, resolve, reject]() {
            try {
                Context::run([&initializer, &resolve]() { initializer(resolve); });
            } catch (...) {
                reject(std::current_exception());
            }
        });
    });
}

}  // namespace service
}  // namespace rili
